function createNewUser() {
    const newUser = {};
    newUser.firstName = prompt("Indicate First Name, please:");
    newUser.lastName = prompt("Indicate Last Name, please:");
    newUser.birthday = prompt("Indicate date of birth\n in the following order (dd.mm.yyyy):");
  
    newUser.getLogin = function() {
      return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase());
    };
  
    newUser.getAge = function() {
      const dateParts = this.birthday.split('.');
      const day = parseInt(dateParts[0], 10);
      const month = parseInt(dateParts[1], 10) - 1;
      const year = parseInt(dateParts[2], 10);
      const today = new Date();
      const birthDate = new Date(year, month, day);
      let age = today.getFullYear() - birthDate.getFullYear();
      const monthChange = today.getMonth() - birthDate.getMonth();
      if (monthChange < 0 || (monthChange === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    };
  
    newUser.getPassword = function() {
      const firstLetter = this.firstName.charAt(0).toUpperCase();
      const lastNameLower = this.lastName.toLowerCase();
      const year = this.birthday.split(".")[2];
      return firstLetter + lastNameLower + year;
    };
  
    return newUser;
  }
  
  const user = createNewUser();
  console.log("USER:", user.firstName, user.lastName);
  console.log("USER LOGIN:", user.getLogin());
  console.log("USER's AGE:", user.getAge(), "years");
  console.log("USER's PASSWORD:", user.getPassword());
  